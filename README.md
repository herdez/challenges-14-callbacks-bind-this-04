## Challenges 14 (Instructions) – Solving Problems 

### Bind, this & Callbacks 

Steps
- Clone repo or download.

Deriverables
- Upload the github/gitlab repository url to Trello.

Final result

Our client has decided that it is important to use `bind()` method and `callbacks` functions to solve the problem and he has given us some tests to prove our functions. The statements/declarations in the console.log(…) describe the expected output from the function when provided with a given input and should evaluate to true if you have written the function correctly.

> You must use `binding`, `this` & `callbacks`. 