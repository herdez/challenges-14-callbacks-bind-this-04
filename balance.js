//this, bind & callbacks

/*
Define a function `getMonthlyBalance` to get monthly account balance. This 
function will be reused for different accounts. 

Account example:

```
Marco has a balance of $2000 and a monthly expense of $1900:

const marco = {name: 'Marco Alvarado', total: 2000}
```

***Constraints:
*
* - The `getMonthlyBalance()` function can be used again and again to get current
*   balance from account every month.
* - The same `getMonthlyBalance()` function can be used for another account with
*   a different monthly expense.
*

Define a function `getMonthlyExpense` to get a total of monthly expenses.

Marco´s monthly expenses example:

```
const marcoExpenses = [1000, 200, 600, 100]
```

***Constraints:
*
* - Only use helper 'reduce' method.
*

Also, define the following functions using 'bind()' method, so all tests 
must be true:

- getMarcoBalance().
- getMayraBalance().

*/


//+++ YOUR CODE GOES HERE


//define getMonthlyExpense()



//define getMonthlyBalance()




/**Marco's Data */

//Marco expenses list
const marcoExpenses1 = [100, 200, 150, 50]
const marcoExpenses2 = [100, 200]

//Marco's account 
const marco = {name: "Marco Alvarado", total: 2000}

//define getMarcoBalance()




/**Mayra's Data */

//Mayra expenses list
const mayraExpenses = [100, 200, 150, 50]

//Mayra's account 
const mayra = {name: "Mayra Saucedo", total: 1000}

//define getMayraBalance()





// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~* Tests (Don't Touch) *~*~*~*~*~*~*~*~*
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*



//'getMarcoBalance()' test - ****Don't Touch****
console.log("Test 1... ", getMarcoBalance(marcoExpenses1, getMonthlyExpense) === "Marco Alvarado remaining balance: 1500")
console.log("Test 2... ", getMarcoBalance(marcoExpenses1, getMonthlyExpense) === "Marco Alvarado remaining balance: 1000")
console.log("Test 3... ", getMarcoBalance(marcoExpenses1, getMonthlyExpense) === "Marco Alvarado remaining balance: 500")
console.log("Test 4... ", getMarcoBalance(marcoExpenses2, getMonthlyExpense) === "Marco Alvarado remaining balance: 200")

//'getMayraBalance()' test - ****Don't Touch****
console.log("Test 5... ", getMayraBalance(mayraExpenses, getMonthlyExpense) === "Mayra Saucedo remaining balance: 500")
console.log("Test 6... ", getMayraBalance(mayraExpenses, getMonthlyExpense) === "Mayra Saucedo remaining balance: 0")


